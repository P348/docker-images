# Dockerfile for p348reco, based on Alma Linux 9 environment
# Provides default environment for p348reco builds

FROM gitlab-registry.cern.ch/linuxsupport/alma9-base

RUN dnf upgrade -y --refresh
RUN dnf install -y epel-release
RUN dnf -qy update

# Packages needed for LCG on CVMFS SFT:
RUN dnf -qy install sudo which glibc-langpack-en
RUN dnf -qy install git libtirpc libicu libglvnd-glx pcre2-devel libXrender libSM make \
      glibc-devel expat-devel mesa-libGLU-devel
# Packages specific for P348-daq
RUN dnf -qy install gcc-gfortran expat-devel boost-devel root root-physics \
    root-montecarlo-eg root-smatrix root-graf3d-eve root-gdml root-genvector \
    eigen3-devel
# Packages needed by p348reco's CI/CD tests
RUN yum -qy install cppcheck cloc
