# Dockerfile for p348reco, based on CentOS7 environment
# Provides default environment for p348reco builds

FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

RUN yum -q -y install gcc-gfortran expat-devel boost-devel root root-physics \
    root-montecarlo-eg root-smatrix root-graf3d-eve root-gdml root-genvector \
    eigen3-devel
RUN yum -q -y install cppcheck cloc
